﻿using System;

namespace AndaForceUtils.Variables
{
	public class ObservableVariable<T> 
	{
		public event Action<T> OnValueChanged;
		
		public T Value 
		{
			get
			{
				return _value;
			}
			set
			{
				if (_prevValue.Equals(value)) return;
							
				_value = value;
				_prevValue = value;
				
				if (OnValueChanged!=null)
				{
					OnValueChanged.Invoke(_value);
				}
			}
		}
		private T _value;
		private T _prevValue;
	}

}