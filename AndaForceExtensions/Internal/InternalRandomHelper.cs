﻿using AndaForceUtils.Random;

namespace AndaForceUtils.Internal
{
    internal class InternalRandomHelper
    {
        internal static System.Random Rnd = new System.Random();
        internal static IRandom Random = new SystemRandom();
    }
}