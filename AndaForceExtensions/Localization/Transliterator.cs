﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AndaForceUtils.Localization
{
	public class Transliterator
	{
		private const int RussianLettersCount = 32;

		private readonly List<String> Russian = new List<String>() 
			{
				"А","Б","В","Г","Д","Е","Ж","З","И","Й","К","Л","М","Н","О","П","Р","С","Т","У","Ф","Х","Ч","Ц","Ш","Щ","Ъ","Ы","Ь","Э","Ю","Я",
				"а","б","в","г","д","е","ж","з","и","й","к","л","м","н","о","п","р","с","т","у","ф","х","ч","ц","ш","щ","ъ","ы","ь","э","ю","я",
			};

		private readonly List<String> English = new List<String>() 
			{
				"А","B","V","G","D","E","ZH","Z","I","I","K","L","M","N","O","P","R","S","T","U","F","H","CH","TS","SH","SCH","","Y","","E","IU","YA",
				"a","b","v","g","d","e","zh","z","i","i","k","l","m","n","o","p","r","s","t","u","f","h","ch","ts","sh","sch","","y","","e","iu","ya",
			};

		private readonly Dictionary<String, String> LettersDictionary;

		public Transliterator()
		{
			LettersDictionary = new Dictionary<String, String>();
			for (int i = 0; i < RussianLettersCount * 2; i++)
			{
				LettersDictionary.Add(Russian[i], English[i]);
			}
		}

		public String Transliterate(String source)
		{
			var stringBuilder = new StringBuilder();

			for (int i = 0; i < source.Length; i++)
			{
				var letter = source[i].ToString();
				if (LettersDictionary.ContainsKey(letter))
				{
					stringBuilder.Append(LettersDictionary[letter]);
				}
				else
				{
					stringBuilder.Append(letter);
				}
			}

			return stringBuilder.ToString();
		}
	}
}

