﻿using AndaForceUtils.Internal;
using AndaForceUtils.Random;
using System.Collections.Generic;

namespace AndaForceUtils.Collections.Generic.Extension.List
{
	public static partial class ListExtension
    {
        #region Random 

        public static TValue GetRandomItem<TValue>(this List<TValue> list)
        {
            return GetRandomItem(list, InternalRandomHelper.Random);
        }

        public static TValue GetRandomItem<TValue>(this List<TValue> list, IIntRandom random )
        {
            if (list.Count > 0)
            {
                return list[random.NextInt(0, list.Count)];
            }
            return default(TValue);
        }

        public static TValue RemoveRandomItem<TValue>(this List<TValue> list)
        {
            return RemoveRandomItem<TValue>(list, InternalRandomHelper.Random);
        }

        public static TValue RemoveRandomItem<TValue>(this List<TValue> list, IIntRandom random)
        {
            if (list.Count > 0)
            {
                var value = list[random.NextInt(0, list.Count)];
                list.Remove(value);

                return value;
            }
            return default(TValue);
        }

        #endregion
    }
}