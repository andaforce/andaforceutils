﻿using System.Drawing;

namespace AndaForceUtils.Math.Geometry
{
    public static class GeometryHelper
    {
        public static float PointLineSegmentIntersection(PointF linePoint1, PointF linePoint2, PointF point)
        {
            return (point.X - linePoint1.X) * (point.Y - linePoint2.Y) - (point.Y - linePoint1.Y) * (point.X - linePoint2.X);
            // 0 if given point intersects given line
            // >0 point is to the right, <0 points is to the left
        }
        public static int PointLineSegmentIntersection(Point linePoint1, Point linePoint2, Point point)
        {
            return (point.X - linePoint1.X) * (point.Y - linePoint2.Y) - (point.Y - linePoint1.Y) * (point.X - linePoint2.X);
        }
        
        public static bool IsPointIntersectsLineSegment_MeasurementError(PointF linePoint1, PointF linePoint2, PointF point, float allowableMeasurementError = 0)
        {
            return System.Math.Abs(PointLineSegmentIntersection(linePoint1, linePoint2, point)) <= allowableMeasurementError;
            // checks if point is within "allowed area" of line to be considered as intercrossing
        }
        public static bool IsPointIntersectsLineSegment_MeasurementError(Point linePoint1, Point linePoint2, Point point, int allowableMeasurementError = 0)
        {
            return System.Math.Abs(PointLineSegmentIntersection(linePoint1, linePoint2, point)) <= allowableMeasurementError;
        }
        public static bool IsPointIntersectsLineSegment(PointF linePoint1, PointF linePoint2, PointF point)
        {
            return  PointLineSegmentIntersection(linePoint1, linePoint2, point) == 0.0f;
            // freaking possible loss
        }
        public static bool IsPointIntersectsLineSegment(Point linePoint1, Point linePoint2, Point point)
        {
            return PointLineSegmentIntersection(linePoint1, linePoint2, point) == 0;
        }



    }
}
