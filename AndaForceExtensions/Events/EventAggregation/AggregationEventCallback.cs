﻿using System;

namespace AndaForceUtils.Events.EventAggregation
{
	internal class AggregationEventCallback<TEventArgs> : IComparable<AggregationEventCallback<TEventArgs>>
		where TEventArgs : AggregationEventArgs
	{
		public int Priority { get; private set; }
		public bool IsDelayed { get; private set; }

		private Action<TEventArgs> Callback;
		private TEventArgs _callbackArgs = null;

		internal AggregationEventCallback(Action<TEventArgs> callback, int priority = 0, bool isDelayed = false)
		{
			Callback = callback;
			Priority = priority;
		}

		internal void Raise(TEventArgs args)
		{
			if (IsDelayed)
			{
				_callbackArgs = args;
			}
			else
			{
				Callback.Invoke(args);
			}
		}

		internal void RaiseDelayed()
		{
			if (IsDelayed && _callbackArgs != null)
			{
				Callback.Invoke(_callbackArgs);
				_callbackArgs = null;
			}
		}

		public override bool Equals(object obj)
		{
			return this.Callback == (obj as AggregationEventCallback<TEventArgs>).Callback;
		}
			
		public override int GetHashCode()
		{
			return this.Callback.GetHashCode();
		}

		#region IComparable implementation

		public int CompareTo(AggregationEventCallback<TEventArgs> other)
		{
			return other.Priority.CompareTo(Priority);
		}

		#endregion
	}
}

