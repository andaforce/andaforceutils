﻿using System;
using System.Collections.Generic;

namespace AndaForceUtils.Events.EventAggregation
{
	public class EventAggregator
	{
		#region Singleton

		private static readonly EventAggregator _instance = new EventAggregator();

		static EventAggregator()
		{
		}

		private EventAggregator()
		{
		}

		public static EventAggregator Instance
		{
			get { return _instance; }
		}

		#endregion
       
        private readonly Dictionary<String, object> _eventsCollection = new Dictionary<String, object>();

		#region Public methods

		public void RegisterEvent<TEventType, TEventArgs>() 
			where TEventType : AggregationEvent<TEventArgs>, new()
			where TEventArgs : AggregationEventArgs
		{
			var key = GetKey<TEventType>();
			if (_eventsCollection.ContainsKey(key))
			{
				throw new ArgumentException(String.Format("The event type [{0}] is already registered", key));
			}

			_eventsCollection.Add(key, new TEventType());
		}

		public void Subscribe<TEventType, TEventArgs>(Action<TEventArgs> callback, int subscribeId) 
			where TEventType : AggregationEvent<TEventArgs>, new()
			where TEventArgs : AggregationEventArgs
		{
			GetEvent<TEventType, TEventArgs>().Subscribe(callback);
		}

		public void Unsubscribe<TEventType, TEventArgs>(Action<TEventArgs> callback) 
			where TEventType : AggregationEvent<TEventArgs>, new()
			where TEventArgs : AggregationEventArgs
		{
			GetEvent<TEventType, TEventArgs>().Unsubscribe(callback);
		}

		public void Publish<TEventType, TEventArgs>(TEventArgs args)
			where TEventType : AggregationEvent<TEventArgs>, new()
			where TEventArgs : AggregationEventArgs
		{
			GetEvent<TEventType, TEventArgs>().Raise(args);
		}

		public void NofityDelayed()
		{
			foreach(var someEvent in _eventsCollection)
			{
				(someEvent.Value as IAggregationEvent).RaiseDelayed();
			}
		}

		#endregion

		#region Private methods

		private String GetKey<T>()
		{
			return typeof(T).GetType().ToString();
		}

		private TEventType GetEvent<TEventType, TEventArgs>()
			where TEventType : AggregationEvent<TEventArgs>, new()
			where TEventArgs : AggregationEventArgs
		{
			var key = GetKey<TEventType>();
			if (_eventsCollection.ContainsKey(key))
			{
				return _eventsCollection[key] as TEventType;
			}

			throw new KeyNotFoundException(string.Format("The event type [{0}] is not registered", key));
		}

		#endregion
	}
}

