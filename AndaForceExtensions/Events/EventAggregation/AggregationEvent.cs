﻿using System;
using System.Collections.Generic;
using System.Linq;
using AndaForceUtils.Collections.Generic.Extension.List;

namespace AndaForceUtils.Events.EventAggregation
{
	public abstract class AggregationEvent<TEventArgs> : IAggregationEvent
		where TEventArgs : AggregationEventArgs
	{
		private readonly List<AggregationEventCallback<TEventArgs>> Callbacks = new List<AggregationEventCallback<TEventArgs>>();

		public void Subscribe(Action<TEventArgs> callback, int priority = 0, bool isDelayed = false)
		{
			Callbacks.Add(new AggregationEventCallback<TEventArgs>(callback, priority, isDelayed));
			Callbacks.QuickSort((a, b) => a.CompareTo(b));
		}
      
		public bool Unsubscribe(Action<TEventArgs> callback)
		{
			var checkCallback = Callbacks.FirstOrDefault(a => a.Equals(callback));

			return Callbacks.Remove(checkCallback);
		}

		public void ClearAll()
		{
            Callbacks.Clear();
		}
	
        public void Raise(TEventArgs args)
		{
			Callbacks.ForEach(a => a.Raise(args));
		}

		public void RaiseDelayed()
		{
			Callbacks
				.Where(a => a.IsDelayed)
				.ToList()
				.ForEach(a => a.RaiseDelayed());
		}
	}

	internal interface IAggregationEvent
	{
		void RaiseDelayed();
	}
}

