﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace AndaForceUtils.Serialization
{
    public class XMLSerializator
    {
        #region Load

        public static T Load<T>(
            String loadPath,
            Action<String> onError = null,
            Action<String> onSuccess = null)
            where T : IXMLSerializatorObject, new()
        {
            try
            {
                var reader = new StreamReader(loadPath);
                var instance = DeserializeObject<T>(reader, loadPath, onError, onSuccess);
                reader.Close();

                return instance;
            }
            catch (Exception e)
            {
                ReportError("Load", onError, e);

                return CreateDefaultObject<T>();
            }
        }

        public static T LoadFromString<T>(
            String source,
			Action<String> onError = null,
			Action<String> onSuccess = null) where T : IXMLSerializatorObject, new()
        {
            var reader = new StringReader(source);
            var instance = DeserializeObject<T>(reader, "Raw", onError, onSuccess);
            reader.Close();

            return instance;
        }

        #endregion

        #region Save

        public static void Save<T>(
            T configurationObject,
            String savePath,
			Action<String> onSuccess = null,
			Action<String> onError = null) where T : IXMLSerializatorObject, new()
        {
            try
            {
                var serializer = new XmlSerializer(typeof (T));
                TextWriter writer = new StreamWriter(savePath);
                serializer.Serialize(writer, configurationObject);
                writer.Close();

                ReportSuccess<T>("Serialization", savePath, onSuccess);
            }
            catch (Exception e)
            {
                ReportError("Serialization", onError, e);
            }
        }

        #endregion

        #region Report

		private static void ReportError(String typeMessage, Action<String> onError, Exception e)
        {
            if (onError != null)
            {
                onError.Invoke(
                    String.Format("{0} error: {1}", typeMessage, e.Message));
            }
        }

		private static void ReportSuccess<T>(String typeMessage, string path, Action<String> onSuccess)
            where T : new()
        {
            if (onSuccess != null)
            {
                onSuccess.Invoke(
                    String.Format("{0} completed with {1} (source info: {2})", typeMessage, typeof (T), path));
            }
        }

        #endregion

        #region Base Methods

        private static T DeserializeObject<T>(
            TextReader reader,
            String sourceInfo = "Raw",
			Action<String> onError = null,
			Action<String> onSuccess = null) where T : IXMLSerializatorObject, new()
        {
            try
            {
                var serializer = new XmlSerializer(typeof (T));
                var instance = (T) serializer.Deserialize(reader);

                ReportSuccess<T>("Deserialization", sourceInfo, onSuccess);

                return instance;
            }
            catch (Exception e)
            {
                if (onError != null)
                {
                    ReportError("Deserialization", onError, e);
                }
                return CreateDefaultObject<T>();
            }
        }

        private static T CreateDefaultObject<T>() where T : IXMLSerializatorObject, new()
        {
            var instance = new T();
            instance.InitDefault();

            return instance;
        }

        #endregion
    }

    public interface IXMLSerializatorObject
    {
        void InitDefault();
    }
}