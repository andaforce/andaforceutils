﻿
namespace AndaForceUtils.Random
{
    public interface IIntRandom
    {
        int NextInt();
        int NextInt(int maxValue);
        int NextInt(int minValue, int maxValue);
    }
}
