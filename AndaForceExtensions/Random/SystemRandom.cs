﻿
namespace AndaForceUtils.Random
{
    public class SystemRandom : IRandom
    {
        System.Random random = new System.Random();

        public int NextInt()
        {
            return random.Next();
        }

        public int NextInt(int maxValue)
        {
            return random.Next(maxValue);
        }

        public int NextInt(int minValue, int maxValue)
        {
            return random.Next(minValue, maxValue);
        }
    }
}